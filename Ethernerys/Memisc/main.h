#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <Windows.h>
#include <iomanip>
#include <Psapi.h>
#include <Shlwapi.h>

#ifdef _WIN32
#include <TlHelp32.h>


#pragma comment(lib, "Shlwapi")
#elif _WIN64

#endif

DWORD_PTR findSubsequence(BYTE*, SIZE_T, BYTE*, SIZE_T);
DWORD_PTR findBaseAddress(HANDLE hProcess, PWCHAR moduleName);
PIMAGE_SECTION_HEADER getSegmentInfo(HANDLE hProcess, PCHAR segmentName);
PIMAGE_NT_HEADERS getNTHead(LPVOID header);

const wchar_t *GetWC(const char *c);
void toLower(PWCHAR str);
void writeAddrToMem(DWORD_PTR addr, PBYTE mem);