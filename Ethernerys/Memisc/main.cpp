#include "main.h"

void toLower(PWCHAR str) {
	while(*str!=0) {
		*(WCHAR*)str = *str > 0x40 && *str < 0x5B ? *(WCHAR*)str ^  0x20 : *str;
		str++;
	}
}

DWORD_PTR findSubsequence(BYTE* pattern, SIZE_T patternSize, 
					BYTE* buff, SIZE_T bufferSize) {
	int j = 0;
	for(int i = 0; i < bufferSize; i++, j = 0) {
		for(; j < patternSize && pattern[j] == buff[i + j]; j++);
		if (j == patternSize) return i;
	}	
	return 0;
}



PIMAGE_SECTION_HEADER getSegmentInfo(HANDLE hProcess, PCHAR segmentName) {
	SIZE_T testSize = 0x1000;
	LPVOID header = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, testSize);
	if(!header)
		return 0;

	DWORD_PTR imageBase = findBaseAddress(hProcess, L"geth.exe");
	if(!ReadProcessMemory(hProcess, (LPCVOID)imageBase, header, testSize, &testSize))
		return 0;
	
	DWORD_PTR imageSectionStart = findSubsequence((BYTE*)".text", 5, (BYTE*)header, 0x1000);
	if(!imageSectionStart) 
		return 0;

	SIZE_T nameLen = strlen(segmentName);
	for(PIMAGE_SECTION_HEADER psh = (PIMAGE_SECTION_HEADER)(imageSectionStart + (DWORD_PTR)header); *(BYTE*)psh; psh += sizeof(IMAGE_SECTION_HEADER))
		if(strcmp(segmentName, (const char*)psh->Name) == 0)
			return psh;

	return NULL;
	
}


DWORD_PTR findBaseAddress(HANDLE hProcess, PWCHAR moduleName) {


	DWORD  processID_     = NULL ;
	DWORD_PTR  processBaseAddress_   = 0;

#ifndef _WIN64
    HANDLE moduleSnapshotHandle_ = INVALID_HANDLE_VALUE;
    MODULEENTRY32 moduleEntry_;

	moduleSnapshotHandle_ = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, 
		GetProcessId(hProcess));

    if (moduleSnapshotHandle_ == INVALID_HANDLE_VALUE)
        return 0;

    moduleEntry_.dwSize = sizeof( MODULEENTRY32 );
    if( !Module32First( moduleSnapshotHandle_, &moduleEntry_ ) )
    {        
        CloseHandle( moduleSnapshotHandle_ );    
        return 0;
    }

	if (moduleName == NULL) {
		moduleName = new WCHAR[MAX_PATH];
		GetModuleFileNameExW(hProcess, NULL, moduleName, MAX_PATH);
		moduleName = PathFindFileNameW(moduleName);
	}

    do
    {
		if( !wcscmp(moduleEntry_.szModule, moduleName) )
			processBaseAddress_ = (unsigned int)moduleEntry_.modBaseAddr;            
	} while( Module32Next( moduleSnapshotHandle_, &moduleEntry_ ) 
		&& processBaseAddress_ == 0 );

#elif _WIN64
	HMODULE     *moduleArray;
    LPBYTE      moduleArrayBytes;
    DWORD       bytesRequired;
	if ( EnumProcessModules( hProcess, NULL, 0, &bytesRequired ) )
		if ( bytesRequired )
            {
                moduleArrayBytes = (LPBYTE)LocalAlloc( LPTR, bytesRequired );

                if ( moduleArrayBytes )
                {
                    unsigned int moduleCount;

                    moduleCount = bytesRequired / sizeof( HMODULE );
                    moduleArray = (HMODULE *)moduleArrayBytes;

                    if ( EnumProcessModules( hProcess, moduleArray, bytesRequired, &bytesRequired ) )
                    {
						WCHAR szModName[MAX_PATH];
						
						for(int i = 0; i < moduleCount; i ++)  {
							GetModuleBaseNameW(hProcess, moduleArray[i], szModName, MAX_PATH);
							toLower(szModName);
							if(wcscmp(szModName, moduleName) == 0) {
								processBaseAddress_ = (DWORD_PTR)moduleArray[i];
								break;
							}
						}                        
                    }
                    LocalFree( moduleArrayBytes );
                }
            }
        	
#endif

    return processBaseAddress_;
}

const wchar_t *GetWC(const char *c)
{
    const size_t cSize = strlen(c)+1;
    wchar_t* wc = new wchar_t[cSize];
    mbstowcs (wc, c, cSize);

    return wc;
}

PIMAGE_NT_HEADERS getNTHead(LPVOID header){
	return (PIMAGE_NT_HEADERS)((DWORD_PTR)header + ((IMAGE_DOS_HEADER*)header)->e_lfanew);
}

void writeAddrToMem(DWORD_PTR addr, PBYTE mem) {
	DWORD_PTR shiftL = 0xFF;
	for(DWORD_PTR i = 0; i < sizeof(DWORD_PTR); i++)
		mem[i] = (addr & (shiftL << (i * 8))) >> (i * 8);
}